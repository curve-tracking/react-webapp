// import React, { useState } from 'react';
// import Paper from '@material-ui/core/Paper';
// import { makeStyles } from '@material-ui/core/styles';
// import { Box } from '@material-ui/core';

// const useStyles = makeStyles((theme) => ({
//   paper: {
//     padding: theme.spacing(1),
//     color: 'dark',
//   },
//   paddingChild: {
//     paddingTop: '2%',
//   },
// }));

// const PoolCard = ({ pool }) => {
//   const [poolApys, setPoolApys] = useState([]);
//   const { address, apy_base, token, lpToken, balance } = pool;

//   const fetchPoolApys = (address) =>
//     fetch(`https://api.curve.fi/api/getApys?address=${address}`)
//       .then((response) => response.json())
//       .then((datas) => {});

//   fetchPoolApys(address);

//   const classes = useStyles();
//   return (
//     <div className={classes.paddingChild}>
//       <Paper className={classes.paper}>
//         <Box>
//           <Box>{`${token.symbol}-${lpToken.symbol} | ${balance}$ TVL`}</Box>
//           {/* <Box>{`${apy_base}% | ${apy_crv}$ CRV | ${apy_cvx}% CVX`}</Box> */}
//         </Box>
//         {/* Use this to display stuff like live APY */}
//       </Paper>
//     </div>
//   );
// };

// export default PoolCard;
