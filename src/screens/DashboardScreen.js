import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Loader from 'react-loader-spinner';
import { makeStyles } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import PoolPositionCard from '../components/dashboard/PoolPositionCard';

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: theme.spacing(2),
  },
}));

const DashboardScreen = () => {
  const navigation = useHistory();
  const classes = useStyles();
  const error = useSelector((store) => store.pools.error);
  const isLoading = useSelector((store) => store.userPositions.isLoadingUserPositions);
  const poolsPositions = useSelector((store) => store.userPositions.positions);

  useEffect(() => {
    if (error !== null) {
      navigation.goBack();
    }
  }, [navigation, error]);

  const displayPoolPositions = () =>
    poolsPositions.map(
      (poolPosition) =>
        poolPosition.currentPosition !== null && (
          <PoolPositionCard key={poolPosition.name} poolPosition={poolPosition} />
        )
    );

  return (
    <Grid
      className={classes.container}
      item
      xs={12}
      lg={12}
      container
      direction="column"
      justifyContent="flex-start"
      alignItems="center">
      {isLoading ? (
        <Loader type="Puff" color="#00BFFF" height={100} width={100} />
      ) : (
        <Grid item xs={12} lg={12}>
          {displayPoolPositions()}
        </Grid>
      )}
    </Grid>
  );
};

export default DashboardScreen;
