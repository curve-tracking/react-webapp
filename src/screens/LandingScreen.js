import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { Box, Typography } from '@material-ui/core';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';

const ETH_ADDRESS_SCHEMA = Yup.object().shape({
  address: Yup.string()
    .trim()
    .matches(/^0x[a-fA-F0-9]{40}$/, 'Invalid Ethereum address.')
    .required('Address required.'),
});

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
  },
  header: {
    marginBottom: 25,
  },
  errorMessage: {
    color: theme.palette.text.error,
  },
  addressInput: {
    minWidth: 420,
  },
  subtitle: {
    marginBottom: '2%',
  },
}));

const LandingScreen = () => {
  const navigation = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const savedAddress = useSelector((store) => store.app.userAddress);
  const isUsingDefaultDatas = useSelector((store) => store.app.isUsingDefaultDatas);
  const error = useSelector((store) => store.pools.error);

  useEffect(() => {
    if ((savedAddress !== null || isUsingDefaultDatas) && error === null) {
      navigation.push('/dashboard');
    }
  }, [navigation, savedAddress, isUsingDefaultDatas, error]);

  const onSubmit = ({ address }) => {
    dispatch({ type: 'SAVE_USER_ADDRESS', userAddress: address });
    navigation.push('/dashboard');
  };

  return (
    <Grid
      className={classes.container}
      item
      xs={12}
      lg={12}
      container
      direction="column"
      justifyContent="flex-start"
      alignItems="center">
      <Box className={classes.header}>
        <Typography variant="h2">Welcome to Curve Tracker !</Typography>
      </Box>
      <Typography className={classes.subtitle} variant="subtitle1">
        Please provide your Ethereum address below
      </Typography>
      <Formik
        initialValues={{
          address: '',
        }}
        validationSchema={ETH_ADDRESS_SCHEMA}
        onSubmit={onSubmit}>
        {({ values, errors, touched, handleChange, handleSubmit }) => (
          <Form className={classes.addressForm}>
            <TextField
              value={values.address}
              className={classes.addressInput}
              id="address"
              name="address"
              label="Address"
              type="search"
              variant="outlined"
              onChange={handleChange}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton aria-label="search field" onClick={handleSubmit} edge="end">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            {errors.address && touched.address && values.address !== '' && (
              <Box className={classes.errorMessage}>{errors.address}</Box>
            )}
          </Form>
        )}
      </Formik>
    </Grid>
  );
};

export default LandingScreen;
