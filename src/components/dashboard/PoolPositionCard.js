import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import UserPositionCard from './UserPositionCard';

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: '2%',
  },
  paper: {
    padding: theme.spacing(1),
  },
}));
const PoolPositionCard = ({ poolPosition }) => {
  const { name, currentPosition } = poolPosition;
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <Paper className={classes.paper}>
        <Typography>{`${name} | 1 bn TVL | 28,4% APY`}</Typography>
        {/* Use this to display stuff like live APY */}
        <UserPositionCard name={name} currentPosition={currentPosition} />
      </Paper>
    </Box>
  );
};

PoolPositionCard.defaultProps = {
  poolPosition: null,
};

PoolPositionCard.propTypes = {
  poolPosition: PropTypes.shape({
    name: PropTypes.string,
    currentPosition: PropTypes.shape({
      description: PropTypes.string,
      lpTokens: PropTypes.number,
      accruedFees: PropTypes.number,
      tokens: PropTypes.arrayOf({
        token: PropTypes.string,
        numTokens: PropTypes.number,
        valueTokens: PropTypes.number,
        coingeckoPrice: PropTypes.number,
      }),
      rewards: PropTypes.arrayOf({
        token: PropTypes.string,
        numTokens: PropTypes.number,
        valueTokens: PropTypes.number,
        coingeckoPrice: PropTypes.number,
      }),
    }),
  }),
};

export default PoolPositionCard;
