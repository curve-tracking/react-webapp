// Initial State
const initialState = {
  poolPosition: null,
  isLoadingPoolPosition: false,
  error: null,
};
// Redux: Pools Reducer
const poolPosition = (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_USER_ADDRESS':
    case 'SAVE_USER_ADDRESS':
      return {
        ...initialState,
        error: null,
      };
    case 'POOL_POSITION_REQUESTED':
      return {
        ...state,
        isLoadingPoolPosition: true,
        error: null,
      };
    case 'POOL_POSITION_LOADED':
      return {
        ...state,
        poolPosition: action.poolPosition,
        isLoadingPoolPosition: false,
        error: null,
      };
    case 'POOL_POSITION_ERROR':
      return {
        ...state,
        isLoadingPoolPosition: false,
        error: action.error,
      };
    default:
      return state;
  }
};
// Exports
export default poolPosition;
