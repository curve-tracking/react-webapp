import React from 'react';
import { Typography } from '@material-ui/core';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';

const FormatedNumber = ({ number, prefix }) => (
  <NumberFormat
    value={number}
    className="foo"
    displayType="text"
    thousandSeparator
    prefix={prefix}
    renderText={(value) => <Typography>{value}</Typography>}
  />
);

FormatedNumber.defaultProps = {
  number: 0.0,
  prefix: '',
};

FormatedNumber.propTypes = {
  number: PropTypes.number,
  prefix: PropTypes.string,
};

export default FormatedNumber;
