import { put, select, takeLeading } from 'redux-saga/effects';
import tricryptoHistoricalPosition from '../ressources/tricrypto_historical_position.json';

// eslint-disable-next-line func-names
const loadPoolPosition = function* ({ name }) {
  try {
    const positionSummary = yield select(
      (store) =>
        store.userPositions.positions.filter((poolPosition) => poolPosition.name === name)[0]
    );
    const finalPosition = {
      ...positionSummary.currentPosition,
      ...tricryptoHistoricalPosition,
    };
    yield put({
      type: 'POOL_POSITION_LOADED',
      poolPosition: finalPosition,
    });
  } catch (error) {
    yield put({
      type: 'POOL_POSITION_ERROR',
      error: `POOL_POSITION_ERROR - ${error.toString()}`,
    });
  }
};

// eslint-disable-next-line func-names
export const watchPoolPositionRequests = function* () {
  yield takeLeading('POOL_POSITION_REQUESTED', loadPoolPosition);
};

export default watchPoolPositionRequests;
