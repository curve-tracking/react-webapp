import { call, put, select, takeLeading } from 'redux-saga/effects';

const fetchPoolsPositions = (address) =>
  fetch(`http://127.0.0.1:5000/summary/${address}`).then((response) => response.json());

// eslint-disable-next-line func-names
const loadUserPositions = function* () {
  try {
    const address = yield select((store) => store.app.userAddress);
    if (address === null) {
      yield put({
        type: 'POOLS_POSITIONS_ERROR',
        error: 'Please enter an ETH address',
      });
    } else {
      const poolsPositions = yield call(fetchPoolsPositions, address);
      yield put({
        type: 'POOLS_POSITIONS_LOADED',
        poolsPositions,
      });
    }
  } catch (error) {
    yield put({
      type: 'POOLS_POSITIONS_ERROR',
      error: `POOLS_POSITIONS_ERROR - ${error.toString()}`,
    });
  }
};

// eslint-disable-next-line func-names
export const watchUserPositionsRequests = function* () {
  yield takeLeading('USER_POSITIONS_REQUESTED', loadUserPositions);
};

export default watchUserPositionsRequests;
