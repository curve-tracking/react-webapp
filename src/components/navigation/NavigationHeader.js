import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Modal from 'react-modal';
import { Box } from '@material-ui/core';
import CurrenciesModal from '../Common/CurrenciesModal';
import ProfileModal from '../Common/ProfileModal';
import LinksList from './LinksList';

const defaultModalStyle = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '2%',
  },
};

const darkModalStyles = {
  content: {
    ...defaultModalStyle.content,
    backgroundColor: '#424242',
  },
};

const lightModalStyles = {
  content: {
    ...defaultModalStyle.content,
    backgroundColor: 'white',
  },
};

const useStyles = makeStyles((theme) => ({
  mainNav: {
    padding: theme.spacing(1),
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
}));

const NavigationHeader = () => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const dispatch = useDispatch();
  const navigation = useHistory();
  const [currenciesModalOpen, setCurrenciesModalOpen] = React.useState(false);
  const [profileModalOpen, setProfileModalOpen] = React.useState(false);

  const useDarkMode = useSelector((store) => store.app.isUsingDarkMode);
  const isUsingDefaultDatas = useSelector((store) => store.app.isUsingDefaultDatas);
  const selectedCurrency = useSelector((store) => store.app.currency);

  const resetUserAddress = () => dispatch({ type: 'RESET_USER_ADDRESS' });
  const switchToDefaultDatas = () => dispatch({ type: 'USE_DEFAULT_DATAS' });
  const removeDefaultDatas = () => dispatch({ type: 'REMOVE_DEFAULT_DATAS' });

  const switchUseDefaultDatas = () => {
    if (isUsingDefaultDatas) {
      removeDefaultDatas();
      navigation.push('/');
    } else {
      resetUserAddress();
      switchToDefaultDatas();
      navigation.push('/dashboard');
    }
  };

  const openCurrenciesModal = () => {
    if (profileModalOpen) {
      setProfileModalOpen(false);
    }
    setCurrenciesModalOpen(true);
  };
  const closeCurrenciesModal = () => {
    setCurrenciesModalOpen(false);
  };
  const openProfileModal = () => {
    if (currenciesModalOpen) {
      setCurrenciesModalOpen(false);
    }
    setProfileModalOpen(true);
  };
  const closeProfileModal = () => {
    setProfileModalOpen(false);
  };

  return (
    <nav className={classes.mainNav}>
      <FormGroup>
        <FormControlLabel
          control={
            <Switch
              checked={isUsingDefaultDatas}
              onChange={switchUseDefaultDatas}
              aria-label="login switch"
            />
          }
          label={isUsingDefaultDatas ? 'Using default datas' : 'Using API'}
        />
      </FormGroup>
      <AppBar position="static">
        <Toolbar>
          <Typography>Curve-Tracker</Typography>
          <LinksList />
          <Box>
            <IconButton
              aria-label="currency used by app"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={openCurrenciesModal}
              color="inherit">
              <Typography>{selectedCurrency.toUpperCase()}</Typography>
            </IconButton>
            <Modal
              isOpen={currenciesModalOpen}
              onRequestClose={closeCurrenciesModal}
              style={useDarkMode ? darkModalStyles : lightModalStyles}
              contentLabel="Currencies Modal">
              <CurrenciesModal />
            </Modal>
          </Box>
          <Box>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={openProfileModal}
              color="inherit">
              <AccountCircle />
            </IconButton>
            <Modal
              isOpen={profileModalOpen}
              onRequestClose={closeProfileModal}
              style={useDarkMode ? darkModalStyles : lightModalStyles}
              contentLabel="Profile Modal">
              <ProfileModal closeProfileModal={closeProfileModal} />
            </Modal>
          </Box>
        </Toolbar>
      </AppBar>
    </nav>
  );
};

export default NavigationHeader;
