import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Loader from 'react-loader-spinner';
import { Box } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const PoolsScreen = () => {
  const dispatch = useDispatch();
  const navigation = useHistory();

  const error = useSelector((store) => store.pools.error);
  const isUsingDefaultDatas = useSelector((store) => store.app.isUsingDefaultDatas);
  const isLoading = useSelector((store) => store.pools.isLoadingPools);
  const pools = useSelector((store) => store.pools.pools);

  useEffect(() => {
    if (pools === null && isUsingDefaultDatas) {
      dispatch({ type: 'DEFAULT_POOLS_LIST_REQUESTED' });
    }
    if (pools === null) {
      dispatch({ type: 'POOLS_LIST_REQUESTED' });
    }
  }, [dispatch, pools, isUsingDefaultDatas]);

  useEffect(() => {
    if (error !== null) {
      navigation.goBack();
    }
  }, [navigation, error]);

  return (
    <Box>
      {isLoading ? (
        <Loader type="Puff" color="#00BFFF" height={100} width={100} />
      ) : (
        <Grid>
          <Box>Convex pools list</Box>
          <Box>WIP</Box>
          {/* {pools.map((pool) => (
            <PoolCard key={pool.address} pool={pool} />
          ))} */}
        </Grid>
      )}
    </Box>
  );
};

export default PoolsScreen;
