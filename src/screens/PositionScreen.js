import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Loader from 'react-loader-spinner';
import clsx from 'clsx';
import { makeStyles, Paper, Typography } from '@material-ui/core';
import { useParams } from 'react-router-dom';
import PoolPositionChart from '../components/Common/PoolPositionChart';
import AssetsTable from '../components/Common/Tables/AssetsTable';
import FormatedNumberWithCurrency from '../components/Common/FormatedNumberWithCurrency';
import { calculateEstimatedPositionValue } from '../helpers/utils';
import HistoryTable from '../components/Common/Tables/HistoryTable';

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  breakdownContainer: {
    marginTop: theme.spacing(2),
  },
  smallGridContainer: {
    display: 'flex',
  },
  smallGridPaper: {
    display: 'flex',
    flexGrow: 1,
    paddingTop: theme.spacing(5),
    paddingBottom: theme.spacing(5),
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  lpTokensPaper: {},
  estValuePaper: {},
  smallPaperTitle: {
    fontWeight: 'bold',
  },
}));

const PositionScreen = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { name } = useParams();

  const isLoading = useSelector((store) => store.pools.isLoadingPoolPosition);
  const poolPosition = useSelector((store) => store.poolPosition.poolPosition);

  useEffect(() => {
    if (poolPosition === null) {
      dispatch({ type: 'POOL_POSITION_REQUESTED', name });
    }
  }, [dispatch, poolPosition, name]);

  if (isLoading || poolPosition === null) {
    return (
      <Grid className={classes.container} item xs={12} lg={12}>
        <Loader type="Puff" color="#00BFFF" height={100} width={100} />
      </Grid>
    );
  }

  const hasRewards = !!poolPosition.rewards;
  const hasClaimed = !!poolPosition.historicalPosition.claimedRewards;

  return (
    <Grid className={classes.container} item xs={12} lg={12}>
      <Typography component="h2">{`Breakdown of ${name} position`}</Typography>
      <PoolPositionChart />
      <Grid container spacing={2} className={classes.breakdownContainer}>
        <Grid item lg={2} xs={4} className={classes.smallGridContainer}>
          <Paper className={clsx(classes.smallGridPaper, classes.lpTokensPaper)}>
            <Typography className={classes.smallPaperTitle}>LP Tokens</Typography>
            <Typography>{poolPosition.lpTokens}</Typography>
            <Typography className={classes.smallPaperTitle}>Estimated value</Typography>
            <FormatedNumberWithCurrency
              number={calculateEstimatedPositionValue(poolPosition.tokens)}
            />
            <Typography className={classes.smallPaperTitle}>Pool TVL</Typography>
            <FormatedNumberWithCurrency number={449507900} />
          </Paper>
        </Grid>
        <Grid item lg={2} xs={4} className={classes.smallGridContainer}>
          <Paper className={clsx(classes.smallGridPaper, classes.estValuePaper)}>
            <Typography className={classes.smallPaperTitle}>Pool native APY</Typography>
            <Typography>6,02%</Typography>
            <Typography className={classes.smallPaperTitle}>Pool CRV APY</Typography>
            <Typography>29,33%</Typography>
            <Typography className={classes.smallPaperTitle}>Pool gauge weight</Typography>
            <Typography>18,44%</Typography>
          </Paper>
        </Grid>
        <Grid item lg={2} xs={4} className={classes.smallGridContainer}>
          <Paper className={clsx(classes.smallGridPaper, classes.estValuePaper)}>
            <Typography className={classes.smallPaperTitle}>24hr change</Typography>
            <Typography>+14,256 (+4.20%)</Typography>
            <Typography className={classes.smallPaperTitle}>Since joining</Typography>
            <Typography>+29,512 (+8.40%)</Typography>
            <Typography className={classes.smallPaperTitle}>Estim. I/L</Typography>
            <Typography>-3,680 (-2,2%)</Typography>
          </Paper>
        </Grid>
        <Grid item lg={6} xs={12}>
          <AssetsTable title="Single asset withdrawal" tokensList={poolPosition.tokens} />
        </Grid>
        {(hasRewards || hasClaimed) && (
          <>
            {hasRewards && (
              <Grid item lg={hasClaimed ? 6 : 12} xs={12}>
                <AssetsTable
                  title="Claimable rewards"
                  showTotal
                  tokensList={poolPosition.rewards}
                />
              </Grid>
            )}
            {hasClaimed && (
              <Grid item lg={hasRewards ? 6 : 12} xs={12}>
                <AssetsTable
                  title="Claimed rewards"
                  showTotal
                  tokensList={poolPosition.historicalPosition.claimedRewards[0].rewards}
                />
              </Grid>
            )}
          </>
        )}
        <Grid item lg={12} xs={12}>
          <HistoryTable operations={poolPosition.historicalPosition.operations} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PositionScreen;
