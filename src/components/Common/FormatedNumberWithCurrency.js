import React from 'react';
import { Typography } from '@material-ui/core';
import NumberFormat from 'react-number-format';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

const getPrefixFromCurrency = (currency) => {
  switch (currency) {
    case 'eur':
      return '€';
    case 'usd':
    default:
      return '$';
  }
};

const FormatedNumberWithCurrency = ({ number, textClassName }) => {
  const currency = useSelector((store) => store.app.currency);
  return (
    <NumberFormat
      value={number}
      className="foo"
      displayType="text"
      thousandSeparator
      prefix={getPrefixFromCurrency(currency)}
      renderText={(value) => <Typography className={textClassName}>{value}</Typography>}
    />
  );
};

FormatedNumberWithCurrency.defaultProps = {
  number: 0.0,
  textClassName: '',
};

FormatedNumberWithCurrency.propTypes = {
  number: PropTypes.number,
  textClassName: PropTypes.string,
};

export default FormatedNumberWithCurrency;
