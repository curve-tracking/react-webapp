import React from 'react';
import { createTheme, makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Modal from 'react-modal';
import { ThemeProvider } from '@material-ui/core';
import { useSelector } from 'react-redux';
import NavigationWrapper from './components/navigation/NavigationWrapper';
import LightTheme from './styles/LightTheme';
import DarkTheme from './styles/DarkTheme';

Modal.setAppElement('#root');

const useStyles = makeStyles(() => ({
  mainClass: {
    height: '100vh',
  },
}));

const App = () => {
  const storedIsUsingDarkMode = useSelector((store) => store.app.isUsingDarkMode);
  const appliedTheme = createTheme(storedIsUsingDarkMode ? DarkTheme : LightTheme);
  const classes = useStyles(appliedTheme);

  return (
    <ThemeProvider theme={appliedTheme}>
      <CssBaseline />
      <main className={classes.mainClass}>
        <NavigationWrapper />
      </main>
    </ThemeProvider>
  );
};

export default App;
