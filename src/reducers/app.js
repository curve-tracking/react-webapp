// Initial State
const initialState = {
  currency: 'usd',
  userAddress: null,
  isUsingDefaultDatas: false,
  isUsingDarkMode: false,
  avCurrencies: [
    { value: 'usd', label: 'Dollar' },
    { value: 'eur', label: 'Euro' },
    { value: 'btc', label: 'Bitcoin' },
    { value: 'eth', label: 'Ether' },
  ],
};
// Redux: App Reducer
const app = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_APP_CURRENCY':
      return {
        ...state,
        currency: action.currency,
      };
    case 'SAVE_USER_ADDRESS':
      return {
        ...state,
        userAddress: action.userAddress,
        isUsingDefaultDatas: false,
      };
    case 'RESET_USER_ADDRESS':
      return {
        ...state,
        userAddress: null,
        isUsingDefaultDatas: false,
      };
    case 'CHANGE_COLOR_MODE':
      return {
        ...state,
        isUsingDarkMode: action.isUsingDarkMode,
      };
    case 'USE_DEFAULT_DATAS':
      return {
        ...state,
        isUsingDefaultDatas: true,
      };
    case 'REMOVE_DEFAULT_DATAS':
      return {
        ...state,
        isUsingDefaultDatas: false,
      };
    default:
      return state;
  }
};
// Exports
export default app;
