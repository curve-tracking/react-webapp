// Imports: Dependencies
import { createStore, applyMiddleware } from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import config from './config/config';
// Imports: Redux Root Reducer
import rootReducer from './reducers/index';
// Imports: Redux Root Saga
import rootSaga from './sagas/index';
// Middleware: Redux Saga
const sagaMiddleware = createSagaMiddleware();
// Redux Persist
const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Redux: Store

let appliedMiddlewear = null;
if (config.env === 'dev') {
  appliedMiddlewear = applyMiddleware(sagaMiddleware, createLogger());
} else {
  appliedMiddlewear = applyMiddleware(sagaMiddleware);
}
const store = createStore(persistedReducer, appliedMiddlewear);
// Middleware: Redux Saga
sagaMiddleware.run(rootSaga);
// Exports
const persistor = persistStore(store);

export { store, persistor };
