import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Box, makeStyles, useTheme } from '@material-ui/core';
import LandingScreen from '../../screens/LandingScreen';
import DashboardScreen from '../../screens/DashboardScreen';
import PoolsScreen from '../../screens/PoolsScreen';
import PositionScreen from '../../screens/PositionScreen';
import NavigationHeader from './NavigationHeader';
import NavigationFooter from './NavigationFooter';

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(3),
    paddingBottom: 0,
    display: 'flex',
    flexGrow: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
}));

const NavigationWrapper = () => {
  const theme = useTheme();
  const classes = useStyles(theme);

  return (
    <Router>
      <Box className={classes.container}>
        <NavigationHeader />
        <Switch>
          <Route path="/" exact>
            <LandingScreen />
          </Route>
          <Route path="/dashboard" exact>
            <DashboardScreen />
          </Route>
          <Route path="/pools" exact>
            <PoolsScreen />
          </Route>
          <Route path="/position/:name">
            <PositionScreen />
          </Route>
        </Switch>
        <NavigationFooter />
      </Box>
    </Router>
  );
};
export default NavigationWrapper;
