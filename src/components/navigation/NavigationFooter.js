import React from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles({
  footerContainer: {
    width: '100%',
    minHeight: '9%',
    borderTopColor: 'black',
    borderTopWidth: 1,
    borderTopStyle: 'solid',
  },
});

const NavigationFooter = () => {
  const classes = useStyles();
  return (
    <Box
      className={classes.footerContainer}
      justifyContent="center"
      alignItems="center"
      display="flex">
      <Typography>Built by bout3fiddy, Allen & Clonescody</Typography>
    </Box>
  );
};

export default NavigationFooter;
