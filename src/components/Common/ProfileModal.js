import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Button, Typography } from '@material-ui/core';
import Brightness3Icon from '@material-ui/icons/Brightness3';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  profileModalContentContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: theme.palette.background.paper,
  },
  darkModeButton: {
    padding: 0,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: '2%',
  },
  darkModeText: {
    marginRight: '5%',
  },
}));

const ProfileModal = ({ closeProfileModal }) => {
  const address = useSelector((store) => store.app.userAddress);
  const isUsingDarkMode = useSelector((store) => store.app.isUsingDarkMode);
  const classes = useStyles();

  const dispatch = useDispatch();

  const resetUserAddress = () => dispatch({ type: 'RESET_USER_ADDRESS' });
  const changeColorMode = () =>
    dispatch({ type: 'CHANGE_COLOR_MODE', isUsingDarkMode: !isUsingDarkMode });

  return (
    <Box className={classes.profileModalContentContainer}>
      <Typography variant="subtitle1">Profile menu</Typography>
      <Button className={classes.darkModeButton} onClick={changeColorMode}>
        <Typography variant="span" className={classes.darkModeText}>
          {isUsingDarkMode ? 'Dark Mode  On' : 'Light Mode On'}
        </Typography>
        {isUsingDarkMode ? <Brightness7Icon /> : <Brightness3Icon />}
      </Button>
      {address !== null ? (
        <Link
          to="/"
          onClick={() => {
            resetUserAddress();
            closeProfileModal();
          }}>
          <Typography variant="span" className={classes.title}>
            Logout
          </Typography>
        </Link>
      ) : (
        <Typography variant="span">Logout if not default datas</Typography>
      )}
    </Box>
  );
};

ProfileModal.defaultProps = {
  closeProfileModal: null,
};

ProfileModal.propTypes = {
  closeProfileModal: PropTypes.func,
};

export default ProfileModal;
