// Initial State
const initialState = {
  pools: [],
  poolsPositions: [],
  poolHistory: null,
  isLoadingPools: false,
  isLoadingPoolsPositions: false,
  isLoadingPoolHistory: false,
  error: null,
};
// Redux: Pools Reducer
const pools = (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_USER_ADDRESS':
    case 'SAVE_USER_ADDRESS':
      return {
        ...initialState,
        error: null,
      };
    case 'POOLS_LIST_REQUESTED':
      return {
        ...state,
        isLoadingPools: true,
        error: null,
      };
    case 'POOLS_LIST_LOADED':
      return {
        ...state,
        pools: action.pools,
        isLoadingPools: false,
        error: null,
      };
    case 'POOLS_LIST_ERROR':
      return {
        ...state,
        isLoadingPools: false,
        error: action.error,
      };
    case 'POOLS_POSITIONS_REQUESTED':
      return {
        ...state,
        isLoadingPoolsPositions: true,
        error: null,
      };
    case 'POOLS_POSITIONS_LOADED':
      return {
        ...state,
        isLoadingPoolsPositions: false,
        poolsPositions: action.poolsPositions,
        error: null,
      };
    case 'POOLS_POSITIONS_ERROR':
      return {
        ...state,
        isLoadingPoolsPositions: false,
        error: action.error,
      };
    default:
      return state;
  }
};
// Exports
export default pools;
