import WBTCLogo from '../assets/images/wbtc_logo.png';
import ETHLogo from '../assets/images/eth_logo.png';
import USDTLogo from '../assets/images/usdt_logo.png';
import CVXLogo from '../assets/images/cvx_logo.png';
import CRVLogo from '../assets/images/crv_logo.png';

export const calculateEstimatedPositionValue = (tokens, rewards) => {
  let totalValue = 0.0;
  tokens.forEach(({ valueTokens }) => {
    totalValue += valueTokens;
  });
  totalValue /= tokens.length; // make average of the pool value
  if (rewards) {
    rewards.forEach(({ valueTokens }) => {
      totalValue += valueTokens;
    });
  }
  return totalValue.toFixed(3);
};

export const getLogoForToken = (token) => {
  switch (token) {
    case 'WBTC':
      return WBTCLogo;
    case 'ETH':
      return ETHLogo;
    case 'USDT':
      return USDTLogo;
    case 'CVX':
      return CVXLogo;
    case 'CRV':
    default:
      return CRVLogo;
  }
};
