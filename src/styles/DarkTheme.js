const DarkTheme = {
  palette: {
    type: 'dark',
  },
};

export default DarkTheme;
