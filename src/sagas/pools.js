import { call, put, takeLeading } from 'redux-saga/effects';

const fetchCurvePools = () =>
  fetch(`https://api.curve.fi/api/getFactoryPools`).then((response) => response.json());

// eslint-disable-next-line func-names
const loadPoolsList = function* () {
  try {
    const { success, data } = yield call(fetchCurvePools);
    if (success) {
      yield put({
        type: 'POOLS_LIST_LOADED',
        pools: data.poolData,
      });
    } else {
      yield put({
        type: 'POOLS_LIST_ERROR',
        error: `POOLS_LIST_ERROR - Error on curve fetching}`,
      });
    }
  } catch (error) {
    yield put({
      type: 'POOLS_LIST_ERROR',
      error: `POOLS_LIST_ERROR - ${error.toString()}`,
    });
  }
};

// eslint-disable-next-line func-names
export const watchPoolsRequests = function* () {
  yield takeLeading('POOLS_LIST_REQUESTED', loadPoolsList);
};

export default watchPoolsRequests;
