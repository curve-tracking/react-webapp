import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import { useDispatch, useSelector } from 'react-redux';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: 0,
  },
  modalTitle: {
    marginBottom: '10%',
  },
}));

const CurrenciesModal = () => {
  const availableCurrencies = useSelector((store) => store.app.avCurrencies);
  const selectedCurrency = useSelector((store) => store.app.currency);
  const classes = useStyles();

  const dispatch = useDispatch();

  const switchCurrency = (newCurrency) =>
    dispatch({ type: 'CHANGE_APP_CURRENCY', currency: newCurrency });

  const handleToggle = (value) => () => {
    switchCurrency(value);
  };

  return (
    <List dense className={classes.root}>
      <Typography variant="subtitle1" className={classes.modalTitle}>
        Currencies
      </Typography>
      {availableCurrencies.map(({ label, value }) => (
        <ListItem key={value} button>
          <ListItemText id={value} primary={`${label}`} />
          <ListItemSecondaryAction>
            <Checkbox
              edge="end"
              onChange={handleToggle(value)}
              checked={value === selectedCurrency}
              inputProps={{ 'aria-labelledby': value }}
            />
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
};
export default CurrenciesModal;
