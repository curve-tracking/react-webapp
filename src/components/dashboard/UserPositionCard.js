import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import { Box } from '@material-ui/core';
import PropTypes from 'prop-types';
import { calculateEstimatedPositionValue } from '../../helpers/utils';
import AssetsTable from '../Common/Tables/AssetsTable';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(20),
    flexBasis: '50%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  ulAdjust: {
    paddingLeft: '2%',
  },
  marginAccordion: {
    margin: '1%',
  },
  borderAccAdd: {
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: theme.palette.text.secondary,
  },
  linkText: {
    color: theme.palette.text.primary,
  },
  tokenLogo: {
    width: 25,
    height: 25,
    marginRight: theme.spacing(1),
  },
}));

const UserPositionCard = ({ name, currentPosition }) => {
  console.log('current pos : ', currentPosition);

  const { tokens, rewards, lpTokens } = currentPosition;
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [estimatedValue, setEstimatedValue] = useState(0.0);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  if (estimatedValue === 0.0) {
    setEstimatedValue(calculateEstimatedPositionValue(tokens, rewards));
  }

  const getPositionTitle = () => {
    let string = '';
    tokens.forEach(({ token }) => {
      string += `${token} `;
    });
    return `${string} | ~ ${estimatedValue}$`;
  };

  return (
    <Box className={clsx(classes.root, classes.marginAccordion)} key={name}>
      <Accordion
        className={classes.borderAccAdd}
        expanded={expanded === 'panel1'}
        onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header">
          <Typography variant="body1" className={clsx(classes.heading, classes.secondaryHeading)}>
            {getPositionTitle()}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={3}>
            <Grid item lg={12} xs={12}>
              <Link to={`position/${name}`}>
                <Typography className={classes.linkText}>View details</Typography>
              </Link>
            </Grid>
            <Grid item lg={3} xs={12}>
              <Typography>LP Tokens</Typography>
            </Grid>
            <Grid item lg={9} xs={12}>
              <Typography>{lpTokens}</Typography>
            </Grid>
            <Grid item lg={3} xs={12}>
              <Typography>Single asset withdraw amount</Typography>
            </Grid>
            <Grid item lg={9} xs={12}>
              <AssetsTable tokensList={tokens} />
            </Grid>
            {rewards && (
              <>
                <Grid item lg={3} xs={12}>
                  <Typography>Claimable rewards</Typography>
                </Grid>
                <Grid item lg={9} xs={12}>
                  <AssetsTable showTotal tokensList={rewards} />
                </Grid>
              </>
            )}
          </Grid>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

UserPositionCard.defaultProps = {
  name: '',
  currentPosition: {
    description: '',
    lpTokens: 0.0,
    accruedFees: 0.0,
    tokens: [],
    rewards: [],
  },
};

UserPositionCard.propTypes = {
  name: PropTypes.string,
  currentPosition: PropTypes.shape({
    description: PropTypes.string,
    lpTokens: PropTypes.number,
    accruedFees: PropTypes.number,
    tokens: PropTypes.arrayOf({
      token: PropTypes.string,
      numTokens: PropTypes.number,
      valueTokens: PropTypes.number,
      coingeckoPrice: PropTypes.number,
    }),
    rewards: PropTypes.arrayOf({
      token: PropTypes.string,
      numTokens: PropTypes.number,
      valueTokens: PropTypes.number,
      coingeckoPrice: PropTypes.number,
    }),
  }),
};

export default UserPositionCard;
