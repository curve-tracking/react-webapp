import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Box, makeStyles, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

import FormatedNumberWithCurrency from '../FormatedNumberWithCurrency';
import FormatedNumber from '../FormatedNumber';
import { getLogoForToken } from '../../../helpers/utils';

const useStyles = makeStyles((theme) => ({
  tableTitle: {
    padding: theme.spacing(2),
  },
  tokenLogo: {
    width: 25,
    height: 25,
    marginRight: theme.spacing(1),
  },
  bold: {
    fontWeight: 'bold',
  },
}));

const getTotalValuesFromTokens = (tokens) => {
  let total = 0.0;
  tokens.forEach(({ valueTokens }) => {
    total += valueTokens;
  });
  return total;
};

const AssetsTable = ({ tokensList, title, size, extended, showHeader, showTotal }) => {
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table size={size} aria-label="simple table">
        {showHeader && (
          <TableHead>
            {title && (
              <Typography variant="subtitle1" className={classes.tableTitle}>
                {title}
              </Typography>
            )}
            <TableRow>
              <TableCell align="left">Asset</TableCell>
              <TableCell align="right">Amount</TableCell>
              {extended && (
                <>
                  <TableCell align="right">Price</TableCell>
                  <TableCell align="right">Total value</TableCell>
                </>
              )}
            </TableRow>
          </TableHead>
        )}
        <TableBody>
          {tokensList.map(
            ({ token, numTokens, coingeckoPrice, valueTokens }) =>
              numTokens > 0 && (
                <TableRow key={token}>
                  <TableCell component="th" scope="row">
                    <Box display="flex" justifyContent="flex-start" alignItems="center">
                      <img
                        className={classes.tokenLogo}
                        src={getLogoForToken(token)}
                        alt={`${token} logo`}
                      />
                      <Typography>{token}</Typography>
                    </Box>
                  </TableCell>
                  <TableCell align="right">
                    <FormatedNumber number={numTokens} />
                  </TableCell>
                  {extended && (
                    <>
                      <TableCell align="right">
                        <FormatedNumberWithCurrency number={coingeckoPrice} />
                      </TableCell>
                      <TableCell align="right">
                        <FormatedNumberWithCurrency number={valueTokens} />
                      </TableCell>
                    </>
                  )}
                </TableRow>
              )
          )}
          {showTotal && (
            <TableRow key="total">
              <TableCell component="th" scope="row">
                <Typography className={classes.bold}>Total</Typography>
              </TableCell>
              <TableCell align="right" />
              <TableCell align="right" />
              <TableCell align="right">
                <FormatedNumberWithCurrency
                  textClassName={classes.bold}
                  number={getTotalValuesFromTokens(tokensList)}
                />
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
AssetsTable.defaultProps = {
  tokensList: [],
  title: '',
  size: 'medium',
  extended: true,
  showHeader: true,
  showTotal: false,
};

AssetsTable.propTypes = {
  tokensList: PropTypes.arrayOf({
    token: PropTypes.string,
    numTokens: PropTypes.number,
    valueTokens: PropTypes.number,
    coingeckoPrice: PropTypes.number,
  }),
  title: PropTypes.string,
  size: PropTypes.string,
  extended: PropTypes.bool,
  showHeader: PropTypes.bool,
  showTotal: PropTypes.bool,
};

export default AssetsTable;
