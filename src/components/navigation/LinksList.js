import React from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  navLinksContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  title: {
    flexGrow: 1,
  },
  growPlaceholder: {
    flexGrow: 1,
  },
  link: {
    marginLeft: '2%',
  },
  linkText: {
    color: 'white',
  },
}));

const LinksList = () => {
  const classes = useStyles();

  const userAddress = useSelector((store) => store.app.userAddress);
  const isUsingDefaultDatas = useSelector((store) => store.app.isUsingDefaultDatas);

  const displayNav = userAddress !== null || (userAddress === null && isUsingDefaultDatas);

  return displayNav ? (
    <Box className={clsx(classes.growPlaceholder, classes.navLinksContainer)}>
      <Link to="/dashboard" className={classes.link}>
        <Typography variant="subtitle1" className={classes.linkText}>
          Dashboard
        </Typography>
      </Link>
      <Link to="/pools" className={classes.link}>
        <Typography variant="subtitle1" className={classes.linkText}>
          Pools
        </Typography>
      </Link>
      {userAddress !== null && (
        <Typography variant="subtitle1" className={classes.linkText}>
          {userAddress}
        </Typography>
      )}
    </Box>
  ) : (
    <Box className={classes.growPlaceholder} />
  );
};

export default LinksList;
