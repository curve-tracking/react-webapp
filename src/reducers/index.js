// Imports: Dependencies
import { combineReducers } from '@reduxjs/toolkit';
// Imports: Reducers
import app from './app';
import pools from './pools';
import poolPosition from './poolPosition';
import userPositions from './userPositions';
// Redux: Root Reducer
const rootReducer = combineReducers({
  app,
  pools,
  poolPosition,
  userPositions,
});
// Exports
export default rootReducer;
