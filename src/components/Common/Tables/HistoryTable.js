import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Box, TableHead, Typography } from '@material-ui/core';
import PlayForWorkIcon from '@material-ui/icons/PlayForWork';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt';
import LaunchRoundedIcon from '@material-ui/icons/LaunchRounded';
import PropTypes from 'prop-types';

import moment from 'moment';
import FormatedNumberWithCurrency from '../FormatedNumberWithCurrency';
import AssetsTable from './AssetsTable';

const usePaginationStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

const TablePaginationActions = ({ count, page, rowsPerPage, onPageChange }) => {
  const classes = usePaginationStyles();
  const theme = useTheme();

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page">
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page">
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page">
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
};

TablePaginationActions.defaultProps = {
  count: 0,
  page: 0,
  rowsPerPage: 5,
  onPageChange: null,
};

TablePaginationActions.propTypes = {
  count: PropTypes.number,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  onPageChange: PropTypes.func,
};

const useHistoryStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
  },
  table: {
    minWidth: 500,
  },
  operationTypeLabel: {
    marginLeft: theme.spacing(1),
  },
  etherscanLink: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  link: {
    color: theme.palette.text.primary,
    marginRight: theme.spacing(1),
  },
}));

const HistoryTable = ({ operations }) => {
  const classes = useHistoryStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, operations.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const getContentForOperationType = (type) => {
    let icon = null;
    let text = '';
    switch (type) {
      case 'remove_liq':
        icon = <ArrowUpwardIcon style={{ color: 'red' }} />;
        text = 'Removed liquidity';
        break;
      case 'claim_pool_rewards':
        icon = <SystemUpdateAltIcon style={{ color: 'orange' }} />;
        text = 'Claimed pools rewards';
        break;
      case 'add_liq':
      default:
        icon = <PlayForWorkIcon style={{ color: 'green' }} />;
        text = 'Added liquidity';
    }
    return (
      <Box display="flex" justifyContent="flex-start" alignItems="center">
        {icon}
        <Typography className={classes.operationTypeLabel}>{text}</Typography>
      </Box>
    );
  };

  return (
    <TableContainer className={classes.container} component={Paper}>
      <Table className={classes.table} aria-label="custom pagination table">
        <TableHead>
          <Typography variant="subtitle1" className={classes.tableTitle}>
            History
          </Typography>
          <TableRow>
            <TableCell>Type</TableCell>
            <TableCell align="center">Date</TableCell>
            <TableCell align="center">Total</TableCell>
            <TableCell align="center">Assets</TableCell>
            <TableCell align="center">Tx hash</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? operations.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : operations
          ).map(({ date, type, tokens, hash }) => {
            let total = 0.0;
            tokens.forEach(({ valueTokens }) => {
              if (type === 'remove_liq') {
                total -= valueTokens;
              } else {
                total += valueTokens;
              }
            });
            return (
              <TableRow key={hash}>
                <TableCell component="th" scope="row">
                  {getContentForOperationType(type)}
                </TableCell>
                <TableCell align="center">{moment(date).format('MM-DD-YYYY HH:MM')}</TableCell>
                <TableCell align="center">
                  <FormatedNumberWithCurrency number={total} />
                </TableCell>
                <TableCell align="right">
                  <AssetsTable
                    showHeader={false}
                    extended={false}
                    size="small"
                    tokensList={tokens}
                  />
                </TableCell>
                <TableCell align="right">
                  <Typography className={classes.etherscanLink}>
                    <a
                      href={`https://etherscan.io/tx/${hash}`}
                      target="_blank"
                      rel="noreferrer"
                      className={classes.link}>
                      Etherscan
                    </a>
                    <LaunchRoundedIcon />
                  </Typography>
                </TableCell>
              </TableRow>
            );
          })}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={3}
              count={operations.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

HistoryTable.defaultProps = {
  operations: [],
};

HistoryTable.propTypes = {
  operations: PropTypes.arrayOf({
    date: PropTypes.number,
    type: PropTypes.string,
    hash: PropTypes.string,
    tokens: PropTypes.arrayOf({
      token: PropTypes.string,
      numTokens: PropTypes.number,
      valueTokens: PropTypes.number,
      coingeckoPrice: PropTypes.number,
    }),
  }),
};

export default HistoryTable;
