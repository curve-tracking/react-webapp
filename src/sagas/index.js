import { all } from 'redux-saga/effects';
import { watchPoolsRequests } from './pools';
import { watchPoolPositionRequests } from './poolPosition';
import { watchUserPositionsRequests } from './userPositions';

function* rootSaga() {
  yield all([watchPoolsRequests(), watchPoolPositionRequests(), watchUserPositionsRequests()]);
}

export default rootSaga;
