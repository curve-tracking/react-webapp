import userPositionsList from '../ressources/user_positions_list.json';

// Initial State
const initialState = {
  positions: [],
  isLoadingUserPositions: false,
  error: null,
};
// Redux: Pools Reducer
const userPositions = (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_USER_ADDRESS':
    case 'REMOVE_DEFAULT_DATAS':
      return {
        ...initialState,
      };
    case 'USE_DEFAULT_DATAS':
      return {
        ...initialState,
        positions: userPositionsList.positions,
      };
    case 'USER_POSITIONS_REQUESTED':
      return {
        ...state,
        isLoadingUserPosition: true,
        error: null,
      };
    case 'USER_POSITIONS_LOADED':
      return {
        ...state,
        positions: action.positions,
        isLoadingUserPosition: false,
        error: null,
      };
    case 'USER_POSITIONS_ERROR':
      return {
        ...state,
        isLoadingUserPosition: false,
        error: action.error,
      };
    default:
      return state;
  }
};
// Exports
export default userPositions;
