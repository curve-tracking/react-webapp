import React from 'react';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';
import { Box } from '@material-ui/core';

// generates a month of fake datas
const generateSeries = () => {
  const result = [[moment().subtract(30, 'd'), [10000, 10100, 9900, 10050]]];
  for (let i = 1; i < 30; i += 1) {
    const date = moment(result[0][0]).add(i, 'd');
    const values = [];
    // 50% chance to go up/down
    if (Math.random() < 0.5) {
      // use previous close as open
      values.push(result[i - 1][1][3]);
      values.push(result[i - 1][1][3] + Math.random() * (1000 - 50) + 50);
      values.push(values[1] - Math.random() * (500 - 50) + 50);
      values.push(result[i - 1][1][3] + Math.random() * (700 - 50) + 50);
    } else {
      values.push(result[i - 1][1][3]);
      values.push(result[i - 1][1][3] - Math.random() * (1000 - 50) + 50);
      values.push(values[1] + Math.random() * (500 - 50) + 50);
      values.push(result[i - 1][1][3] - Math.random() * (700 - 50) + 50);
    }
    result.push([date, values]);
  }
  return result;
};
// const seriesData = [
//   [timestamp, [open, high, low, close]],
//   [timestamp, [open, high, low, close]],
// ];
const seriesData = generateSeries();

// const seriesDataLinear = [
//   [1538856000000, 75000],
//   [1538856900000, 75500],
//   [1538857000000, 80000],
//   [1538858000000, 80000],
//   [1538859000000, 80000],
//   [1538860000000, 80000],
//   [1538861000000, 80000],
// ];

const options = {
  chart: {
    type: 'candlestick',
    height: 400,
    id: 'candles',
    toolbar: {
      autoSelected: 'pan',
      show: false,
    },
    zoom: {
      enabled: false,
    },
  },
  plotOptions: {
    candlestick: {
      colors: {
        upward: '#3C90EB',
        downward: '#DF7D46',
      },
    },
  },
  xaxis: {
    type: 'datetime',
  },
};

// const seriesBar = [
//   {
//     name: 'volume',
//     data: seriesDataLinear,
//   },
// ];
// const optionsBar = {
//   chart: {
//     height: 160,
//     type: 'bar',
//     brush: {
//       enabled: true,
//       target: 'candles',
//     },
//     selection: {
//       enabled: true,
//       xaxis: {
//         min: moment().subtract(30, 'd'),
//         max: moment(),
//       },
//       fill: {
//         color: '#ccc',
//         opacity: 0.4,
//       },
//       stroke: {
//         color: '#0D47A1',
//       },
//     },
//   },
//   dataLabels: {
//     enabled: false,
//   },
//   plotOptions: {
//     bar: {
//       columnWidth: '80%',
//       colors: {
//         ranges: [
//           {
//             from: -1000,
//             to: 0,
//             color: '#F15B46',
//           },
//           {
//             from: 1,
//             to: 10000,
//             color: '#FEB019',
//           },
//         ],
//       },
//     },
//   },
//   stroke: {
//     width: 0,
//   },
//   xaxis: {
//     type: 'datetime',
//     axisBorder: {
//       offsetX: 13,
//     },
//   },
//   yaxis: {
//     labels: {
//       show: false,
//     },
//   },
// };
const series = [
  {
    data: seriesData,
  },
];

const PoolPositionChart = () => (
  <Box lg={12} xs={12}>
    <ReactApexChart options={options} series={series} type="candlestick" height={400} />
  </Box>
);

export default PoolPositionChart;
